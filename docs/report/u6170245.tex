% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}
\usepackage[left=2.0cm, right=2.0cm, top=1.0cm, bottom=1.0cm]{geometry}


\begin{document}
%
\title{Neuron Network Reduction by Input Feature Encoding and Hidden Neuron Pruning}
%
%\titlerunning{Abbreviated paper title}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Hongbo Zhang}
%
%\authorrunning{F. Author et al.}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{ College of Engineering and Computer Science, Australian National University, Canberra, 2601, Australia
\email{u6170245@anu.edu.au}}
%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
Deeping neuron network becomes main stream of machine learning in 
recent years. However, in spite of it powerful ability, it suffers
from too many parameters and overfitting. Therefore, network reduction
techniques become important. In this paper, two reduction techniques,
including input feature encoding and hidden layer pruning,
are studies in details. When these reduction techniques are
applied to the cover type problem, it can reduce the size of neuron 
network by 50\% without affecting its prediction ability.

\keywords{neuron network reduction \and pruning \and input encoding.}
\end{abstract}
%
%
%
\section{Introduction}

In a typical deeping neuron network, e.g. AlexNet \cite{Krizhevsky:2012},
the number of model parameters is more than 10 millions.
Such huge degree of freedom in a model makes both training and application expensive.
Furthermore, the model are more likely to suffer from overfitting.
Consequently, neuron network reduction becomes important in this case.

In order to reduce the size of neuron network, 
we deploy both feature encoding and 
network pruning techniques.
In fact, there are already lots of researches on the topic of 
feature encoding and network pruning.
For the feature encoding, Bustos and his collabrators
\cite{Bustos:1995} showed a better encoding scheme can 
lead to a better prediction result in GIS problem.
Mikolov \cite{Mikolov:2011} proposed an embedding encoding for language problem.
For the network pruning, Sietsma \cite{Sietsma:1988} published
the seminal work by inspection in two 
stage pruning process.
After Sietsma, many authors proposed their own pruning 
methods, including pruning by relevance \cite{Mozer:1989}, 
contribution \cite{Sanger:1989}, sensitivitly \cite{Karnin:1990}, 
badness \cite{Hagiwara:1990} and finally distinctiveness \cite{Gedeon:1991}.

In this paper, we use a simple all connected forward network with 
only one hidden layer. All units in hidden layer are connected to
all neurons in input layer and output layer. 
There is no connection between input layer and output layer.
The sigmoid non-linear activation function is used in both
hidden layer and output layer.
This simple network is used to study the cover type problem \cite{Blackard:2000}.
Firstly, we perform feature encoding to this problem.
Then,  we prune the hidden neuron layer by distinctiveness,
because of its conceptual clearness.
%We train the neuron network by back-propagation \cite{Rumelhart:1986}.

The structure of paper is organised as following, 
Methods used in this paper, including dataset preprocessing, hyperparameter selection
and pruning by distinctiveness, is introduced in section 2.
In section 3, the detailed implementation is shown.
The results and discussion can be found in section 4.
Finally, it is followed by a conclusion and future work perspective
in section 5.

All the python codes relevant to this work, 
raw dataset and processed dataset can be found
in supplimentary materials.



\section{Methods}

In the first subsection, we introduce the dataset we select,
the reason of selection and the detail of preprocessing the dataset.
The whole dataset is divied into three subsets, training set (50\%),
testing set (25\%) and validation set (25\%).
The dataset is preprocessing by two different encoding scheme,
and a comparison between them is shown in section 4.

In the second subsection, we performed several experiments on
training set and testing set to determined the optimal
hyperparameter, including the size of mini-batch, the number 
of epoches, the learning rate, and so on.
We also make some comparison between training network on GPU and CPU.
In this part, we didn't use validation set at all, to avoid overfitting.

In the third subsection, we introduce the details of pruning by
distinctiveness. In this work, three different kinds of pruning is used,
including removal of neurons with constant output for all patterns,
removal of complementary neuron pairs and removal of one neuron
in similarity neuron pairs.

\subsection{Dataset Selection and Data Preprocessing}

\subsubsection{Why CovType}

We select cover type dataset contributed by Blackard and Dennis \cite{Blackard:2000}.
The dataset can be downloaded at UCI's website 
\footnote{https://archive.ics.uci.edu/ml/datasets/Covertype}.

This data set include $54$ features and $7$ categories.
It is about to predict the plant cover type of an small area give
the area's elevation, aspect, slope, distances to hydrology/roadways/firepoint,
hill shade, soil type and a given wildness region.

We choose this dataset since it's relative large in size among all dataset available at UCI
which will facilitate learning process.
Furthermore, it contains complex input feature, 
which provide lots of space for encoding scheme. 
It contains rich feature type, including continuous, cyclic and categories type.

The oringinal raw dataset is attached as \textit{covtype.data.gz} in supplimentary materials.

The statistical overview of the data set can be found at UCI's website,
or in Blackard's paper \cite{Blackard:2000}, and hence skiped here.
We only list some neccessary information of the dataset 
in Fig.~\ref{fig1} and Tab.~\ref{tab1} below.

In Tab.~\ref{tab1}, features and target are summaries.

\begin{table}[h]
\centering
\caption{Overview of features/target in the dataset cover type. Details of encoding can
	be found in following subsubsections.}
\label{my-label}
\begin{tabular}{|l|l|l|l|}
\hline
Feature/Target Name                    & Type      & Simple Encode & Complex Encode               \\ \hline
Elevation                              & continous & linear squash & linearsquash                 \\ \hline
Aspect                                 & cyclic    & linear squash & cosine and sine              \\ \hline
Slope                                  & continous & linear squash & linearsquash                 \\ \hline
Horizontal\_Distance\_To\_Hydrology    & continous & linear squash & linearsquash                 \\ \hline
Vertical\_Distance\_To\_Hydrology      & continous & linear squash & linearsquash                 \\ \hline
Horizontal\_Distance\_To\_Roadways     & continous & linear squash & linearsquash                 \\ \hline
Hillshade\_9am                         & indexed   & linear squash & one binary and one linear squash \\ \hline
Hillshade\_Noon                        & indexed   & linear squash & one binary and one linear squash \\ \hline
Hillshade\_3pm                         & indexed   & linear squash & linearsquash                 \\ \hline
Horizontal\_Distance\_To\_Fire\_Points & continous & linear squash & linearsquash                 \\ \hline
Wilderness\_Area (4 binary columns)    & category  & one hot       & one hot                      \\ \hline
	Soil\_Type (40 binary columns)         & category  & one hot       & embedding coding\cite{Mikolov:2011}             \\ \hline
Cover\_Type (7 types)                  & category  & un-process    & un-process                   \\ \hline
\end{tabular}
\end{table}

In Fig.~\ref{fig1}, the histogram of non-category type features
are plotted, it will be useful for later encoding.

\begin{figure}
\includegraphics[width=0.85\textwidth]{fig1.eps}
\caption{The histogram of non-category type features. 
	Its distribution will be useful for encoding.} 
\label{fig1}
\end{figure}


\subsubsection{Simple Encoding}

The simple encoding scheme is quite simple and straightforward.
Every feature of continous, indexed or cyclic type is encoded by linear squash,
i,e, normalise to the range $[0,1]$ by linear transformation.
Every category feature is encoded by one-hot encoding scheme.
However, the target cover type is perserved as an integer, which
is category index. Since we will use the \textit{CrossEntropyLoss} object
provided by \textit{pytorch} package, and this object requires 
the target is category index, rather than one hot coding.

We normalise all the continuous, indexed and cyclic type variables to
$[0, 1]$ to increase the learning speed of the neuron network.
Since generally we use an activation function which is only 
sensitive in this range. In fact, the sigmoid function we deployed
in this work is sensitive around $[-1, 1]$, if lots of input features
lay outside this range, the efficiency of learning will decrease 
dramatically. Furthermore, if all the variables are not normalised
to the same range, their variance will be differed by a large mount.
This will lead to slow converge in gradient descent algorithm.

In this encoding scheme, the final number of input feature will
be $54$. This is quite large number for this relative simple 
problem. Such a large number of input neuron will cause the
network has a large number of weights to be determined in back-propagation.
Generally, it will cause the learning slow and also the potential overfitting.

Furthermore, another drawback of this simple encoding is that
soil type takes $40$ one-hot binaries, and they are \textit{sparse}!
With this large number, it isn't $40$ times informative as
other variables.
Such big number will make other input feature, such as slope, sub-dominated.
Consequently, it has lots of redundant information and cause learning slow.


\subsubsection{Complex Encoding}

In the complex encoding scheme, we make lots of improvement.

For cyclic variable ascpect, we use two features, cosine and sin,
to represent its cyclic nature. 

For indexed variables, such as hillshada at 9am and noon 
(the fourth row in Fig.~\ref{fig1}), we use a different encoding scheme.
Since the indexing of these variables are unknown, we 
don't know the detail meaning of the indexes. Hence, 
we cannot apply domain knowledge to these variables.
However, we can inspect its distribution by eyes.
If one watches it closely enough, he/her will find there is
a spike at index equals 255 (rightmost in each figure at 
fourth rwo in Fig.~\ref{fig1}).  Obviously, this index has been 
truncted by unknown reason. 
Consequently, we can make use of this knowledge from observation.
In our encoding scheme, we use two features to represent each index 
variable. One of them is a category variable to reflect whether it is saturated ($\geq 255$) or not.
The other of them is a continous variable normalised to the range $[0,1]$.
For example, for hillshade at 9am indexed variables, if its value is $255$,
we encoded it as $(1, 1.0)$, otherwise, it is encoded as $(0, \textrm{value}/255)$.
If we didn't use this encoding, the difference of index value $254$ and $255$ 
in the linear layer will be very small. However, if we add additional binary feature,
the difference between $254$ and $255$ might be large,
if the weights of this additional binary is large in back-propagation.
However, although the hillshad at 3pm is also indexed variable,
but there is a very small percentage that equals to $255$ (left bottom in Fig.~\ref{fig1}),
hence we just simply encode it by linear squash.

For category type variable soil-type, we use embedding encoding \cite{Mikolov:2011}.
It contains 40 binaries one-hot encoding
in raw data. However, since we know the meaning of these encoding,
we can use the domain knowledge to make things easier.
These one-hot can be converted into US Forest Service (USFS) ELU coding.
The details of their correspondance can be found in the dataset 
description document provided in the supplimentary materials.
The USFS ELU coding contains four digital, 
the first digital represents the climate type,
the second digital represent the geologic type,
the last two digital just represent unit itself.
Consequently, we will use three integer (\textit{climate, geology} and \textit{unit}) to encode the soil type.
All the soil-type variables are converted to 
four digital USFS ELU code firstly, then we take the first digital in
the ELU code as \textit{climate}, the second digital as \textit{geology},
and last two digitals as \textit{unit}. 
If we assume the \textit{geology}, \textit{geology} and \textit{unit}
can be characterised by a latent continuous variable, and this integer
is a representation of such latent variable.
In this case, we can finally normalise these integers to range $[0,1]$.

Notice that the slope variable (2nd row left in Fig.~\ref{fig1}) has
already been categorised by the dataset provider. However,
here we treated it as continous variable, and normalise it to $[0,1]$ by
linear squash.

To the same reason as that in Simple encoding scheme, we
didn't preprocess the target variable covtype.

In Fig.~\ref{fig2}, we shown the histogram of some features in this 
complex encoding scheme.

\begin{figure}
\includegraphics[width=0.85\textwidth]{fig2.eps}
\caption{Histogram of some features, including aspect and soil-type,
	in the complex encoding scheme.} 
\label{fig2}
\end{figure}


In this encoding scheme, there are $20$ input features in total.
This number is reduced by $63\%$. It will reduce the size 
of neuron network significantly. However, we will show later,
it doesn't affect the prediction ability of the network at all.

However, there are still lots of drawback of this encoding scheme.
Encoding cyclic variable into cosine and sin also has drawback,
since the slope near $1$ and $-1$ is small, hence, the resulting
distribution will be more concentrated at $1$ and $-1$.
As shown in Fig.~\ref{fig1}, most data are not uniform distributed
between 0 and 1, and they are clustered in a small region.
This will cause the learning inefficiency. 
Encoding wildness area by one-hot will also enounter the problem of 
sparseness.
There are lots of other drawbacks. We will discuss them in more details
in section 5, and give some potential solutions to these drawbacks.

\subsubsection{Comparison between Simple Encoding and Complex Encoding}
The comparison between the prediction ability of simple encoding and 
complex encoding results are discussed in details in section 4.

\subsubsection{Training Set, Testing Set and Validation Set}

The \textit{covtype} dataset contains $581012$ patterns.
We randomly choose $290506$ of them ($50\%$) as training set,
$145253$ ($25\%$) as testing set and $145253$ ($50\%$) as
validation set.

I have confirmed that the targets in these sets have nearly indentical distribution
by plotting their histogram.

The training set is used in training the neuron network.

The validation set is used to tell the performance of a trained model.

The testing set is used in determining the hyperparameters.
For example, if we want to know which learning rate should
be used in back-propagation, we will perform serveral experiments with 
different learning rate, and select the one gives best accuracy in testing
set. Consequently, testing set is \textit{\textbf{not}} used to tell
performance of a trained model. It is different from a validation set,
and is a part of training by itself.

Finally, I didn't add random noise to the dataset to avoid overfitting.

The training set, testint set and validation set for both
simple encoding and complex encoding can be found in supplimentary materials.



\subsection{Hyperparameter Selection}

In order to determine the hyperparameter in the training,
We train the two layer neuron network by back-propagation \cite{Rumelhart:1986}
with mini-batch gradient descent method using the simple encoding preprocessed dataset
for different combinations of hyperparameters.

We use Adam optimizer with the default momentum parameter and adaptive learning rate
parameter in pytorch.

The hyperparameters in this model includes batch size, number of epoches and
learning rate. We will discuss their selection by experiments in the following.

\subsubsection{GPU or CPU}

We implement the system in pytorch  so that it can utilise GPU to do training.
However, due to the experiments result. The performance of GPU is even
worse than the CPU. The experiment showed that the time used in one epoch training
costs 6 seconds by CPU, but 36 seconds by GPU.

Throughout the training process by GPU, the GPU utilisation flunctuates around $25\%$.
The main bottleneck of it is dataloader in mini-batch training.
Another bottleneck is batch size of a mini-batch cannot be too large, since
the size of whole dataset is not that big.

Hence, we use CPU in the following work.

\subsubsection{Number of Hidden Neuron}

We fix the number of hidden neuron as 100 throughout the work.
Since the purpose of this work is pruning, i,e, reducing the
number of hidden neuron by pruning techniques. Therefore,
we will not fine tuning the size of hidden layer here.
So we just randmly pick a number large enough, say 100, for our problem,
and we will reduce it by a systematically treatment.

\subsubsection{Learning Rate}

We fix the number of epoch to $10$, batch size $290$ and number
of hidden neuron $100$ and perform a series of experiments with
different value of learning rate, and compare the accuracy of the model
on testing set. Notice that we \textit{\textbf{only}} use the 
training set and testing set here. Validation set is \textit{\textbf{never}}
used.

The results are shown in Tab.~\ref{tab2}. 
Consequently, we select learning rate as $0.01$ in this work.

\begin{table}[h]
\centering
\caption{Fix the number of epoch to $10$, batch size $290$ and number 
	of hidden neuron $100$,  perform a series of experiments with
	different value of learning rate. Obviously, 0.01 gives the optimal 
	results. Here, only training set and testing set are used. 
	Validation set is never used here.}
\label{tab2}
\begin{tabular}{|l|l|l|l|l|l|}
	\hline
	learning rate & 0.05 & 0.01 & 0.005 & 0.001 & 0.0005 \\ \hline
	accuracy of testing set & 70\% & 72\% & 69\%  & 65\%  & 65\%   \\ \hline
\end{tabular}
\end{table}

\subsubsection{Batch Size}

We fix the number of epoch to $10$, learning rate $0.01$ and number of hidden 
neuron $100$, and perform a series of experiments with different value of
batch size, and compare the accuracy of the model on testing set.
 Notice that we \textit{\textbf{only}} use the 
training set and testing set here. Validation set is \textit{\textbf{never}}
used.

The results are shown in Tab.~\ref{tab3}.
Consequently, we select batch size as $290$ in this work.
Furthermore, in the case of batch size euqaling 29, the training and 
prediction result fluctuate greatly. That's because the
size of mini batch is so small that the difference of gradient direction
between two mini-batch are large.

\begin{table}[]
\centering
\caption{Fix the number of epoch to $10$, learing rate $0.01$ and number 
	of hidden neuron $100$,  perform a series of experiments with
	different value of batch size. Obviously, 290 gives the optimal 
	results. Here, only training set and testing set are used. 
	Validation set is never used here.}
\label{tab3}
\begin{tabular}{|l|l|l|l|l|}
	\hline
	batch size & 29   & 290  & 2900 & 29000 \\ \hline
	accuracy   & 69\% & 72\% & 67\% & 63\%  \\ \hline
\end{tabular}
\end{table}

\subsubsection{Number of Epoches}

We fix the learning rate $0.01$, batch size $290$ and number of hidden neuron
$100$, and perform a series of experiments with different value of epoch numbers
 to find out when does the loss function of training set and accuracy of testing
 set is converge.

 The results are shown in Fig.~\ref{fig3}. 
 $20$ epoches are enough for the system to converge,
 therefore, we take number of epoches as $20$ in this work.

\begin{figure}
\includegraphics[width=1.0\textwidth]{fig3.eps}
\caption{Fix learning rate $0.01$, batch size $290$ and number of hidden neuron $100$,
	perform a series of experiments with different epoch numbers to find out
	when the training is converge. The result show the optimal is 20 epoches.} 
\label{fig3}
\end{figure}


\subsection{Pruning Techniques}

We adopt the distinctiveness pruning approach \cite{Gedeon:1991},
since its conceptual clearness.

In this approach, we first normalise the output of each hidden neuron
to the range $[-0.5, 0.5]$ and represent it in pattern space, i,e,
in a vector with each element is the hidden neuron output for a certain
input pattern. Then we calculate the angle between neuron pairs.
Using this angle, we can tell whether the hidden neuron pair is similar to 
or complementary to each other. 

Recall that the funcationality of neurons in network is actually
"feature transformation", which means a neuron transform
an input feature to another feature. Convolution Neuron Network
gives us the best visualisation of this viewpoint, the output 
of hidden neurons are actually edges, eigen faces, key points,
or corners in an image. If two hidden neurons  always have the
same output, which means these two neurons actually extract identical
features from the input. Therefore, one of them can be safely 
removed. On the contrary, if two hidden neurons always have 
the opposite output, which means these two neurons also
extract identical feature, but in opposite phase and cancel each
other when convey this information to next layer. Hence, both of
them can be safely removed.

There are many pruning categories associating with distinctiveness.
However, in this work, we only focus on the following three categories.

\subsubsection{Neurons with Constant Output}

In the pattern space representation,
a hidden neuron's output is a vector with dimension of pattern number.
If all the elements in the output vector have the same
value (within a given thresholds, of course), 
then this hidden neuron can be safely removed.
For compensating its removal, we add its weight
to the bias. 
Upon removal of this neuron, we also
remove the bias associated with it in input layer.


\subsubsection{Similarity Neuron Pairs}

If the output vector of two hidden neurons are similar to each
other (angle is smaller than a thresholds), then these two neurons have similar funcationality,
hence one of them can be removed. For compensating its removal,
we add its weight to its couterpart in the pair.
Upon removal of this neuron pair, we also
remove the bias associated with it in input layer.

\subsubsection{Complementary Neuron Paris}

If the output vector of two hidden neurons are complementary to 
each other (angle is larger than a thresholds), then these two neurons have similar funcationality,
but in opposite phase. They cancel the other when are conveyed to
next layer. Hence, both of them can be safely removed, and
no weight/bias adjustment in this layer is required.
Upon removal of this neuron, we also
remove the bias associated with it in input layer.


\section{Implementation}

All the python source code can be found in supplimentary materials.

A new class \textit{PruningANN} which extends 
the \textit{pytorch.nn.Module} is implemented.
In this class, there is object method \textit{pruning} 
which will prune the network. This method
calls other three functions \textit{removeAllConstNeuron}, 
\textit{removeAllComplementaryPair} and \textit{removeAllSimilarPair}
. Each function has input parameter, which is thresholds of 
that specific pruning.

We maintain a matrix of object attribute \textit{hiddenOut} 
with size number of pattern by number of hidden neuron.
It stores the output value of a network and will be used in
distinctiveness calculation later.

The overall pruning algorithm can be illustruted by 
Fig.~\ref{fig4}

\begin{figure}
\includegraphics[width=0.8\textwidth]{fig4.eps}
\caption{Pruning algorithm.} 
\label{fig4}
\end{figure}

In Fig.~\ref{fig4}, a circle represents a neuron.
A rectangular represents a matrix, such as bias, weight and output 
matrix of hidden neurons. The size of these matrix is labelled in 
the figure.
The green neuron is the one to be removed,
if it is removed, all green entries in the corresponding matrixs
should be removed as well.
The orange neuron is the one similar to a neuron to be removed,
if its conterpart is removed, the orange entry in the corresponding matrix 
should be updated.
The purple neuron is a constant neuron, if it is removed,
beside all the green entries in corresponding matrix will be removed,
the purple entries in corresponding matrix will be updated as well.


\section{Results and Discussion}

\subsection{Comparison between Different Encoding}

With the hyperparameter, learning rate $0.01$, number of epoch $20$,
batch size $290$, and number of hidden neurons $100$,
we train the two layer network with both simple encoding preprocessed 
dataset and complex encoding preprocessed dataset.

The results are listed in the Tab.~\ref{tab4}.
The results show that complex encoding's validation accuracy 
is higher than that of simple encoding.

\begin{table}[]
\centering
\caption{Comparison between simple encoding and complex encoding}
\label{tab4}
\begin{tabular}{|l|l|l|l|}
	\hline
 & loss function of training set & accuracy of testing test & accuracy of validation set \\ \hline
 simple encoding  & 1.38                          & 72.0\%                   & 62.8\%                     \\ \hline
 complex encoding & 1.41                          & 70.6\%                   & 63.6\%                     \\ \hline
\end{tabular}
\end{table}

Furthermore, the complex encoding suffers less overfitting problem
than that of simple encoding scheme. Since the 
simple encoding's accuracy of testing set is larger than that of 
complex encoding, but the accuracy of validation set is on the contrary.
This behavior is very easy to understand, and we have discussed it in
the method section.

The number of input neurons is reduced by more than $60\%$,
and at the same time, we get a (slightly) better performance.
Consequently, in the following of this work, we
will use complex encoding to study the pruning techniques.

\subsection{Comparison between Full Network and Pruned Network}

We first train the two layer neuron network with the hyperparameter
given in the section 2.

Then we forward the network over the whole training set
for the pruning later. If we don't do this step,
the output of hidden neuron will be only those in the last
mini-batch, and the output has size of batch size ($290$)
in this case. However, we expect the size of output vector
being the number of whole patterns.

In the next, taking the default thresholds of constant neuron being $0.1$,
angle of similarity being $15$ degrees and angle of 
complementary being $165$,
we prune the trained network.
There are 49 neurons being removed.
Among them, 12 are complementary neurons, 37 are similarity neurons.
And there is no constant neuron at all.

The specific number of how many neuron being removed is 
different for each pruning. However, the typical value is 
around 50. And in most cases, there is no constant neuron being
removed at all! Since we use sigmoid activation function here,
and sigmoid function will amplify the difference in input value,
the output neuron will not be constant for most of time.

The prediction results are shown in Tab.~\ref{tab5}

\begin{table}[]
\centering
\caption{Comparison between the full network and pruned network}
\label{tab5}
\begin{tabular}{|l|l|l|}
	\hline
& Full Net & Pruned Net \\ \hline
training accuracy             & 67.5\%   & 67.3\%     \\ \hline
testing accuracy              & 71.1\%   & 71.0\%     \\ \hline
validation accuracy           & 63.6\%   & 63.4\%     \\ \hline
forwarding time for 100 times & 11s      & 22s        \\ \hline
\end{tabular}
\end{table}

The result shows the prediction ability of pruned network
is exactly the same as the full network.
However, the number of neurons is reduced by $50\%$.

We also experiments on the time of forwarding calculation for
both full network and pruned network. The results are also shown
in Tab.~\ref{tab5}. As expected, the forwarding time drops $50\%$,
which is significantly and very impressive.


\subsection{Comparison between Different Pruning Thresholds}

Since there are very few constant neurons in practical applications.
Therefore, we only focus on the effect of angle thresholds in similarity
and complementary calculations.

Tab.~\ref{tab6} shows the results of different thresholds.

\begin{table}[]
\centering
\caption{Comparison between different angle thresholds in pruning.}
\label{tab6}
\begin{tabular}{|l|l|l|l|l|}
\hline
& Full Net & Pruned (15, 165) & Pruned (20, 160) & Pruned (30, 150) \\ \hline
training accuracy   & 67.5\%   & 67.3\%           & 64.5\%           & 59.6\%           \\ \hline
testing accuracy    & 71.1\%   & 71.0\%           & 67.8\%           & 62.9\%           \\ \hline
validation accuracy & 63.6\%   & 63.4\%           & 60.8\%           & 56.1\%           \\ \hline
hidden neurons num  & 100      & 51               & 41               & 25               \\ \hline
\end{tabular}
\end{table}

We take the similarity angle threshold (a pair of neurons is 
regarded as similar to each other if their distinctiveness angle is 
smaller than the thresholds) as 15, 20 and 30, respectively,
and take the complementary angle thresholds (the thresholds for a
pair of neurons who is complementary to the other) as 165, 160 and 150, respectively.

The results show that (15, 165) the optimal choice perserved the prediction ability.
However, the combination (20, 160) is also a good choice without lossing prediction
ability significantly.

\subsection{Comparison with Other's Work}

Blackard and his collabrators \cite{Blackard:2000} also studies the covertype dataset in 
2000. In their work, the prediction accuracy of two layer ANN is $70.58\%$.

They trained their network with momentum gradient descent method with
a MSE loss function. 
They tried several combination of number of input features,
number of hidden neurons, learning rate and momentum rate.
From these experiments, they claimed that 54 (all) input features,
120 hidden neuron, learning rate 0.05 and momentum 0.5 is the optimal 
for the covtype problem. With these settings, the accuracy of
testing data (corresponding to validation data in this work)
is $70.58\%$.

Their result is $7\%$ higher than that in this work. 
However, the size of neuron network is this work is only a 
quarter of that in Blackard's work.

\section{Furture Work and Conclusion}

\subsection{Improved Data Preprocessing}

As mentioned in the section 2.1, there are lots of drawbacks of current
encoding scheme. Lots of improvement can be done in the future.

The continous type data are all clustered in a small region.
 Consequently, we can use another category variable to 
encoding these clustered continous variables. Taking elevation as an example,
if the value of elevation is between, say 2500 and 3500, 
it is encoded as $(1, \textrm{elevation})$, otherwise, it is
encoded as $(0, \textrm{elevation})$.

Alternatively, such continous type data can also be encoded by categorization.
We break the continous data into several categories, each categories is represented
by a category variable.

To solve the sparseness problem of one-hot encoding, 
one can use equilateral coding scheme \cite{MasterT:1993}.

Finally, we should add random noises to dataset to avoid overfitting.

\subsection{Pruning by Distinctiveness}

\subsubsection{Algorithm}

Current algorithm for pruning is a bit slow. 
It will cost 10 minutes for 100 hidden neurons on an intel I5 CPU for laptop.
Dynamics programming can be deployed in the future to facilitate computing 
efficiency. We can store all the distinctiveness information as an object 
attribute and manipulate that attribute when prunes the network.

\subsubsection{Pruning More Categories}

In current work, we only considered the case that complementary between two
hidden neurons, and haven't considered the case with three or more hidden
neuron. In the future work, we can take them into account and find out whether
they are important in real applications.

\subsection{Conclusion}

In this work, we reduce the size of a neuron network by both 
input feature encoding method and hidden neuron pruning method.

The result shows that the input layer has been reduced by $50\%$
by an appropriate input feature encoding, and 
the hidden layer has also been reduced by $50\%$ by distinctiveness pruning.

The size of the network shrinks, the calculation speeds up.
However, the prediction ability is almost un-affected.


%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
 \bibliographystyle{splncs04}
 \bibliography{citation}
%
% \begin{thebibliography}{8}
% \bibitem{ref_article1}
% Author, F.: Article title. Journal \textbf{2}(5), 99--110 (2016)
% \end{thebibliography}
\end{document}
