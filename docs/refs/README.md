List of references

***BP***

+ back propagation algorithm: 
	```
	Rumelhart, DE, Hinton, GE, Williams, RJ,
 	“Learning internal representations by error
 	propagation,” in Rumelhart, DE, McClelland,
 	Parallel distributed processing, Vol. 1, MIT
 	Press, 1986.
	```

***Encoding***

+ equilateral coding
	```
	Masters, T, Practical Neural Network Recipes in C,
	Academic Press, Boston, 1993.
	```

+ an example of encoding tricks
	```
	Bustos, RA and Gedeon, TD “Decrypting Neural Network Data: A GIS Case Study,” 
	Proceedings International Conference on Artificial Neural Networks and Genetic Algorithms (ICANNGA),
	4 pages, Alès, 1995
	```

+ embedding encoding scheme
	```
	Mikolov, T., Deoras, A., Povey, D., Burget, L. & Cernocky, J. Strategies for training 
	large scale neural network language models. 
	In Proc. Automatic Speech Recognition and Understanding 196–201 (2011). 
	```

***Pruning***

+ seminal work on pruning trained netwok by inspection in two stage pruning process:
	```
	Sietsma, J, & Dow, RF, “Neural net pruning -
	why and how,” IJCNN, vol. 1, pp. 325-333,
	1988.
	```
+ pruning by *relevance*
	```
	Mozer, MC, Smolenski, P, “Using relevance to
	reduce network size automatically,”,
	Connection Science, vol. 1, pp. 3-16, 1989.
	```
+ pruning by *contribution*
	```
	Sanger, D, “Contribution analysis: a technique for
	assigning responsibilities to hidden units in
	connectionist networks”, Connection Science,
	vol. 1, pp. 115-138, 1989.
	```
+ pruning by *sensitivity*
	```
	Karnin, ED, “A simple procedure for pruning
	back-propagation trained neural networks,”
	IEEE Transactions on Neural Networks, vol.
	1, pp. 239-242, 1990.
	```
+ pruning by *badness*
	```
	Hagiwara, M, “Novel back propagation algorithm
	for reduction of hidden units and acceleration
	of convergence using artificial selection,”
	IJCNN, vol. 1, pp. 625-630, 1990.
	```
+ pruning by *distinctiveness*
	```
	Gedeon, TD, Harris, D, “Network Reduction
	Techniques,” Proc. Int. Conf. on Neural
	Networks Methodologies and Applications,
	AMSE, vol. 1, pp. 119-126, San Diego,
	1991a. Proc. 9th Ann. Cog. Sci. Soc. Conf. ,
	Seattle, pp. x-y, 1987.
	```

***Other's Result on CovType Dataset***

+ Blackard's result: ANN 70.58%.
	```
	Blackard, Jock A. and Denis J. Dean. 1998. 
	"Comparative Accuracies of Neural Networks and 
	Discriminant Analysis in Predicting Forest Cover
	 Types from Cartographic Variables." 
	Second Southern Forestry GIS Conference. 
	University of Georgia. Athens, GA. Pages 189-199. 
	```




