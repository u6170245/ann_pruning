# Preprocessing Data

+ Simple encoding: just normalise all features to [0,1]
+ Complex encoding:
	- normalise all continuous to [0,1]. *equalise or categories in the feature*
	- for circular value, use sin and cos
	- for indexed value, since it is truncted at 255, using two variables, saturate and value
	- for 40 bits soil type, using 3-d embedding encoding
	- for 4 bits area, use one hot
	- for output, unchange. minus 1 for each category index later in the pytorch code. *equilateral encoding in the futrue*

+ training/tesint set
	- take 50% as training set, 50% as testing set.
	- the pattern distribution should be the same between training set and testing set. however, here we don't care about it. just randomly seperate whole dataset into training and testing.
	- *we should add noise to avoid overfitting, neglect this step*


# Experiments

## Improve GPU utilization

+ increase batch size: BATCHSIZE = 100 and BATCHSIZE = 2900, both 25% utilization.
+ increase number of worker in dataloader: num\_worker = 4, 16. both 25% utilization.
+ more big neuron network: 

## Learning rate

+ LR = 0.01 and 0.001. 
	- accuarcy of 0.01 is 48%
	- **accuarcy of 0.001 is 64%**
	- accuarcy of 0.005 is 61%
	- accuarcy of 0.0005 is 53%
	



# Training Results

## Simple Encoding Training


***Case 1***

```
Simple Encoding
Epoch = 10
BATCHSIZE = 100
LR = 0.01
num_worker = 4
ANN(54, 100, 100, 7)
```

```
Epoch:  0 Step:  0  | train loss: 1.9424  |test accuray:  0.4866233399654396
Epoch:  0 Step:  290  | train loss: 1.4953  |test accuray:  0.36502172072177513
Epoch:  0 Step:  580  | train loss: 1.5153  |test accuray:  0.36502172072177513
Epoch:  0 Step:  870  | train loss: 1.4853  |test accuray:  0.36502172072177513
Epoch:  0 Step:  1160  | train loss: 1.5953  |test accuray:  0.36502172072177513
Epoch:  0 Step:  1450  | train loss: 1.5253  |test accuray:  0.36502172072177513
Epoch:  0 Step:  1740  | train loss: 1.4353  |test accuray:  0.36502172072177513
Epoch:  0 Step:  2030  | train loss: 1.5553  |test accuray:  0.36502172072177513
Epoch:  0 Step:  2320  | train loss: 1.4753  |test accuray:  0.36502172072177513
Epoch:  0 Step:  2610  | train loss: 1.4853  |test accuray:  0.36502172072177513
Epoch:  0 Step:  2900  | train loss: 1.4453  |test accuray:  0.36502172072177513
Epoch:  1 Step:  0  | train loss: 1.4753  |test accuray:  0.36502172072177513
Epoch:  1 Step:  290  | train loss: 1.4853  |test accuray:  0.36502172072177513
Epoch:  1 Step:  580  | train loss: 1.4953  |test accuray:  0.36502172072177513
Epoch:  1 Step:  870  | train loss: 1.4953  |test accuray:  0.36502172072177513
Epoch:  1 Step:  1160  | train loss: 1.4753  |test accuray:  0.36502172072177513
Epoch:  1 Step:  1450  | train loss: 1.5153  |test accuray:  0.36502172072177513
Epoch:  1 Step:  1740  | train loss: 1.5453  |test accuray:  0.36502172072177513
Epoch:  1 Step:  2030  | train loss: 1.5353  |test accuray:  0.36502172072177513
Epoch:  1 Step:  2320  | train loss: 1.5753  |test accuray:  0.36502172072177513
Epoch:  1 Step:  2610  | train loss: 1.4253  |test accuray:  0.36502172072177513
Epoch:  1 Step:  2900  | train loss: 1.5053  |test accuray:  0.36502172072177513
Epoch:  2 Step:  0  | train loss: 1.4753  |test accuray:  0.36502172072177513
Epoch:  2 Step:  290  | train loss: 1.5453  |test accuray:  0.36502172072177513
Epoch:  2 Step:  580  | train loss: 1.4953  |test accuray:  0.36502172072177513
Epoch:  2 Step:  870  | train loss: 1.4853  |test accuray:  0.36502172072177513
Epoch:  2 Step:  1160  | train loss: 1.5153  |test accuray:  0.36502172072177513
Epoch:  2 Step:  1450  | train loss: 1.5753  |test accuray:  0.36502172072177513
Epoch:  2 Step:  1740  | train loss: 1.4853  |test accuray:  0.36502172072177513
Epoch:  2 Step:  2030  | train loss: 1.4553  |test accuray:  0.36502172072177513
Epoch:  2 Step:  2320  | train loss: 1.5453  |test accuray:  0.36502172072177513
Epoch:  2 Step:  2610  | train loss: 1.4653  |test accuray:  0.36502172072177513
Epoch:  2 Step:  2900  | train loss: 1.4153  |test accuray:  0.36502172072177513
Epoch:  3 Step:  0  | train loss: 1.5253  |test accuray:  0.36502172072177513
Epoch:  3 Step:  290  | train loss: 1.4953  |test accuray:  0.36502172072177513
Epoch:  3 Step:  580  | train loss: 1.5453  |test accuray:  0.36502172072177513
Epoch:  3 Step:  870  | train loss: 1.5053  |test accuray:  0.36502172072177513
Epoch:  3 Step:  1160  | train loss: 1.5153  |test accuray:  0.36502172072177513
Epoch:  3 Step:  1450  | train loss: 1.4953  |test accuray:  0.36502172072177513
Epoch:  3 Step:  1740  | train loss: 1.4853  |test accuray:  0.36502172072177513
Epoch:  3 Step:  2030  | train loss: 1.4753  |test accuray:  0.36502172072177513
Epoch:  3 Step:  2320  | train loss: 1.5572  |test accuray:  0.384873290052529
Epoch:  3 Step:  2610  | train loss: 1.4577  |test accuray:  0.44078263443784294
Epoch:  3 Step:  2900  | train loss: 1.4457  |test accuray:  0.4277364322939974
Epoch:  4 Step:  0  | train loss: 1.4035  |test accuray:  0.4281219664998313
Epoch:  4 Step:  290  | train loss: 1.4554  |test accuray:  0.4254438806771633
Epoch:  4 Step:  580  | train loss: 1.4666  |test accuray:  0.48469222666657485
Epoch:  4 Step:  870  | train loss: 1.4248  |test accuray:  0.5427254514536705
Epoch:  4 Step:  1160  | train loss: 1.4375  |test accuray:  0.5318995132630652
Epoch:  4 Step:  1450  | train loss: 1.3829  |test accuray:  0.5031772149284353
Epoch:  4 Step:  1740  | train loss: 1.3999  |test accuray:  0.48663366677452446
Epoch:  4 Step:  2030  | train loss: 1.3618  |test accuray:  0.5314279223148575
Epoch:  4 Step:  2320  | train loss: 1.4493  |test accuray:  0.5296585956916553
Epoch:  4 Step:  2610  | train loss: 1.4439  |test accuray:  0.5397823108644916
Epoch:  4 Step:  2900  | train loss: 1.4612  |test accuray:  0.5068810971201971
Epoch:  5 Step:  0  | train loss: 1.4311  |test accuray:  0.5007538570631932
Epoch:  5 Step:  290  | train loss: 1.4110  |test accuray:  0.4967918046443103
Epoch:  5 Step:  580  | train loss: 1.4285  |test accuray:  0.5348839610885834
Epoch:  5 Step:  870  | train loss: 1.4280  |test accuray:  0.5140616717038546
Epoch:  5 Step:  1160  | train loss: 1.3919  |test accuray:  0.5163783192085534
Epoch:  5 Step:  1450  | train loss: 1.3839  |test accuray:  0.5200718745912305
Epoch:  5 Step:  1740  | train loss: 1.3767  |test accuray:  0.5344984268827494
Epoch:  5 Step:  2030  | train loss: 1.4663  |test accuray:  0.4095027297198681
Epoch:  5 Step:  2320  | train loss: 1.4193  |test accuray:  0.5329012137442944
Epoch:  5 Step:  2610  | train loss: 1.3629  |test accuray:  0.5333934583106718
Epoch:  5 Step:  2900  | train loss: 1.4003  |test accuray:  0.5369114579389066
Epoch:  6 Step:  0  | train loss: 1.3639  |test accuray:  0.5369045733995167
Epoch:  6 Step:  290  | train loss: 1.3955  |test accuray:  0.5693823879713328
Epoch:  6 Step:  580  | train loss: 1.4057  |test accuray:  0.535162784933874
Epoch:  6 Step:  870  | train loss: 1.3949  |test accuray:  0.5343710629040364
Epoch:  6 Step:  1160  | train loss: 1.3852  |test accuray:  0.5308633900848864
Epoch:  6 Step:  1450  | train loss: 1.4219  |test accuray:  0.46832423426710634
Epoch:  6 Step:  1740  | train loss: 1.4149  |test accuray:  0.5450696371159287
Epoch:  6 Step:  2030  | train loss: 1.3949  |test accuray:  0.5288634313921227
Epoch:  6 Step:  2320  | train loss: 1.4285  |test accuray:  0.5297136720067744
Epoch:  6 Step:  2610  | train loss: 1.3847  |test accuray:  0.5361816967635781
Epoch:  6 Step:  2900  | train loss: 1.4211  |test accuray:  0.5232249936318011
Epoch:  7 Step:  0  | train loss: 1.4375  |test accuray:  0.5232938390257
Epoch:  7 Step:  290  | train loss: 1.4537  |test accuray:  0.5381954245351215
Epoch:  7 Step:  580  | train loss: 1.4421  |test accuray:  0.5123336523169917
Epoch:  7 Step:  870  | train loss: 1.4029  |test accuray:  0.509634912876154
Epoch:  7 Step:  1160  | train loss: 1.4139  |test accuray:  0.5443157800527356
Epoch:  7 Step:  1450  | train loss: 1.3921  |test accuray:  0.5479301632324289
Epoch:  7 Step:  1740  | train loss: 1.4260  |test accuray:  0.5349837869097368
Epoch:  7 Step:  2030  | train loss: 1.3919  |test accuray:  0.5211493050057486
Epoch:  7 Step:  2320  | train loss: 1.3675  |test accuray:  0.5352178612489932
Epoch:  7 Step:  2610  | train loss: 1.3931  |test accuray:  0.5279099226866226
Epoch:  7 Step:  2900  | train loss: 1.4419  |test accuray:  0.5079654120741052
Epoch:  8 Step:  0  | train loss: 1.4157  |test accuray:  0.5095901633701198
Epoch:  8 Step:  290  | train loss: 1.3937  |test accuray:  0.4990705871823646
Epoch:  8 Step:  580  | train loss: 1.3821  |test accuray:  0.52998905358237
Epoch:  8 Step:  870  | train loss: 1.3947  |test accuray:  0.5331490571623305
Epoch:  8 Step:  1160  | train loss: 1.4065  |test accuray:  0.531076810805973
Epoch:  8 Step:  1450  | train loss: 1.4155  |test accuray:  0.5044921619519046
Epoch:  8 Step:  1740  | train loss: 1.4385  |test accuray:  0.5320268772417781
Epoch:  8 Step:  2030  | train loss: 1.3385  |test accuray:  0.5254177194274817
Epoch:  8 Step:  2320  | train loss: 1.4003  |test accuray:  0.5433175218412012
Epoch:  8 Step:  2610  | train loss: 1.4511  |test accuray:  0.49517738015738055
Epoch:  8 Step:  2900  | train loss: 1.4721  |test accuray:  0.5347428280310906
Epoch:  9 Step:  0  | train loss: 1.4357  |test accuray:  0.5347565971098703
Epoch:  9 Step:  290  | train loss: 1.4249  |test accuray:  0.5488699028591492
Epoch:  9 Step:  580  | train loss: 1.4252  |test accuray:  0.5388804362044157
Epoch:  9 Step:  870  | train loss: 1.4547  |test accuray:  0.5370904559630438
Epoch:  9 Step:  1160  | train loss: 1.4203  |test accuray:  0.5394862756707263
Epoch:  9 Step:  1450  | train loss: 1.5049  |test accuray:  0.5491487267044398
Epoch:  9 Step:  1740  | train loss: 1.4093  |test accuray:  0.5293074841827707
Epoch:  9 Step:  2030  | train loss: 1.3931  |test accuray:  0.5510213214184905
Epoch:  9 Step:  2320  | train loss: 1.4567  |test accuray:  0.5267016860236966
Epoch:  9 Step:  2610  | train loss: 1.3767  |test accuray:  0.5414036198908112
Epoch:  9 Step:  2900  | train loss: 1.4112  |test accuray:  0.48573867665383846
finish training in  418.70191407203674
```


***Case 2***

```
Simple Encoding
Epoch = 10
BATCHSIZE = 2900
LR = 0.001
num_worker = 4
ANN(54, 100, 100, 7)
```
	
```
Epoch:  0 Step:  0  | train loss: 1.9667  |test accuray:  0.02632992089664241
Epoch:  0 Step:  10  | train loss: 1.8943  |test accuray:  0.4866233399654396
Epoch:  0 Step:  20  | train loss: 1.7724  |test accuray:  0.4866233399654396
Epoch:  0 Step:  30  | train loss: 1.5991  |test accuray:  0.4866233399654396
Epoch:  0 Step:  40  | train loss: 1.5090  |test accuray:  0.4866233399654396
Epoch:  0 Step:  50  | train loss: 1.4941  |test accuray:  0.4866233399654396
Epoch:  0 Step:  60  | train loss: 1.4891  |test accuray:  0.4866233399654396
Epoch:  0 Step:  70  | train loss: 1.4802  |test accuray:  0.4866233399654396
Epoch:  0 Step:  80  | train loss: 1.4735  |test accuray:  0.48806909323731695
Epoch:  0 Step:  90  | train loss: 1.4572  |test accuray:  0.5181992798771798
Epoch:  0 Step:  100  | train loss: 1.4509  |test accuray:  0.5185882563527088
Epoch:  1 Step:  0  | train loss: 1.4457  |test accuray:  0.5185882563527088
Epoch:  1 Step:  10  | train loss: 1.4263  |test accuray:  0.5269116644750883
Epoch:  1 Step:  20  | train loss: 1.4196  |test accuray:  0.5305467012729513
Epoch:  1 Step:  30  | train loss: 1.4230  |test accuray:  0.5304158950245433
Epoch:  1 Step:  40  | train loss: 1.4136  |test accuray:  0.5314451336633322
Epoch:  1 Step:  50  | train loss: 1.4244  |test accuray:  0.5309184664000055
Epoch:  1 Step:  60  | train loss: 1.4294  |test accuray:  0.5314898831693665
Epoch:  1 Step:  70  | train loss: 1.4168  |test accuray:  0.5312730201785849
Epoch:  1 Step:  80  | train loss: 1.4184  |test accuray:  0.531462345011807
Epoch:  1 Step:  90  | train loss: 1.4109  |test accuray:  0.5318409946782511
Epoch:  1 Step:  100  | train loss: 1.4061  |test accuray:  0.5315656131026554
Epoch:  2 Step:  0  | train loss: 1.4213  |test accuray:  0.5316310162268594
Epoch:  2 Step:  10  | train loss: 1.4068  |test accuray:  0.5309081395909206
Epoch:  2 Step:  20  | train loss: 1.4150  |test accuray:  0.5300372453580993
Epoch:  2 Step:  30  | train loss: 1.4132  |test accuray:  0.5309563313666499
Epoch:  2 Step:  40  | train loss: 1.4063  |test accuray:  0.5306534116334947
Epoch:  2 Step:  50  | train loss: 1.4060  |test accuray:  0.5304675290699675
Epoch:  2 Step:  60  | train loss: 1.4015  |test accuray:  0.5313900573482131
Epoch:  2 Step:  70  | train loss: 1.4129  |test accuray:  0.5310974644241427
Epoch:  2 Step:  80  | train loss: 1.4088  |test accuray:  0.5300957639429134
Epoch:  2 Step:  90  | train loss: 1.4033  |test accuray:  0.53094944682726
Epoch:  2 Step:  100  | train loss: 1.4147  |test accuray:  0.530312626933695
Epoch:  3 Step:  0  | train loss: 1.4100  |test accuray:  0.5302816465064405
Epoch:  3 Step:  10  | train loss: 1.4100  |test accuray:  0.5308048715000723
Epoch:  3 Step:  20  | train loss: 1.4181  |test accuray:  0.5302988578549153
Epoch:  3 Step:  30  | train loss: 1.4027  |test accuray:  0.5312936737967546
Epoch:  3 Step:  40  | train loss: 1.4118  |test accuray:  0.5308806014333611
Epoch:  3 Step:  50  | train loss: 1.4101  |test accuray:  0.530439990912408
Epoch:  3 Step:  60  | train loss: 1.4099  |test accuray:  0.5304675290699675
Epoch:  3 Step:  70  | train loss: 1.4130  |test accuray:  0.5308427364667166
Epoch:  3 Step:  80  | train loss: 1.4111  |test accuray:  0.5307842178819027
Epoch:  3 Step:  90  | train loss: 1.4001  |test accuray:  0.5307463529152582
Epoch:  3 Step:  100  | train loss: 1.3992  |test accuray:  0.5306671807122745
Epoch:  4 Step:  0  | train loss: 1.4061  |test accuray:  0.530632758015325
Epoch:  4 Step:  10  | train loss: 1.3994  |test accuray:  0.531145656199872
Epoch:  4 Step:  20  | train loss: 1.4059  |test accuray:  0.5305535858123412
Epoch:  4 Step:  30  | train loss: 1.4038  |test accuray:  0.5306396425547149
Epoch:  4 Step:  40  | train loss: 1.4001  |test accuray:  0.5301370711792528
Epoch:  4 Step:  50  | train loss: 1.4032  |test accuray:  0.5307497951849531
Epoch:  4 Step:  60  | train loss: 1.4028  |test accuray:  0.5305363744638665
Epoch:  4 Step:  70  | train loss: 1.4047  |test accuray:  0.5305501435426463
Epoch:  4 Step:  80  | train loss: 1.4007  |test accuray:  0.5302885310458304
Epoch:  4 Step:  90  | train loss: 1.4101  |test accuray:  0.5301955897640669
Epoch:  4 Step:  100  | train loss: 1.3948  |test accuray:  0.5303573764397292
Epoch:  5 Step:  0  | train loss: 1.4043  |test accuray:  0.530312626933695
Epoch:  5 Step:  10  | train loss: 1.3995  |test accuray:  0.5306499693637997
Epoch:  5 Step:  20  | train loss: 1.4018  |test accuray:  0.5307670065334279
Epoch:  5 Step:  30  | train loss: 1.4023  |test accuray:  0.5307360261061733
Epoch:  5 Step:  40  | train loss: 1.4030  |test accuray:  0.5308324096576319
Epoch:  5 Step:  50  | train loss: 1.3962  |test accuray:  0.5313625191906536
Epoch:  5 Step:  60  | train loss: 1.3987  |test accuray:  0.534264352543493
Epoch:  5 Step:  70  | train loss: 1.3909  |test accuray:  0.5375069705961323
Epoch:  5 Step:  80  | train loss: 1.3953  |test accuray:  0.5393210467253688
Epoch:  5 Step:  90  | train loss: 1.4001  |test accuray:  0.5414896766331848
Epoch:  5 Step:  100  | train loss: 1.4147  |test accuray:  0.5415240993301343
Epoch:  6 Step:  0  | train loss: 1.3940  |test accuray:  0.5416893282754918
Epoch:  6 Step:  10  | train loss: 1.3966  |test accuray:  0.5422504182357679
Epoch:  6 Step:  20  | train loss: 1.3903  |test accuray:  0.5423846667538709
Epoch:  6 Step:  30  | train loss: 1.3929  |test accuray:  0.5437478055530695
Epoch:  6 Step:  40  | train loss: 1.3935  |test accuray:  0.5442538191982266
Epoch:  6 Step:  50  | train loss: 1.3970  |test accuray:  0.5444982203465677
Epoch:  6 Step:  60  | train loss: 1.3883  |test accuray:  0.5463191810151942
Epoch:  6 Step:  70  | train loss: 1.3824  |test accuray:  0.5486874625653171
Epoch:  6 Step:  80  | train loss: 1.3962  |test accuray:  0.5531520863596621
Epoch:  6 Step:  90  | train loss: 1.4031  |test accuray:  0.5696715386257083
Epoch:  6 Step:  100  | train loss: 1.3781  |test accuray:  0.5697610376377769
Epoch:  7 Step:  0  | train loss: 1.3857  |test accuray:  0.561065864388343
Epoch:  7 Step:  10  | train loss: 1.3947  |test accuray:  0.571093196009721
Epoch:  7 Step:  20  | train loss: 1.3925  |test accuray:  0.5789243595657233
Epoch:  7 Step:  30  | train loss: 1.3937  |test accuray:  0.574728232807584
Epoch:  7 Step:  40  | train loss: 1.3946  |test accuray:  0.5741086242624938
Epoch:  7 Step:  50  | train loss: 1.3917  |test accuray:  0.575754029176678
Epoch:  7 Step:  60  | train loss: 1.3960  |test accuray:  0.5840843218384474
Epoch:  7 Step:  70  | train loss: 1.3849  |test accuray:  0.5975263849972118
Epoch:  7 Step:  80  | train loss: 1.3794  |test accuray:  0.5960737471859445
Epoch:  7 Step:  90  | train loss: 1.4071  |test accuray:  0.6006003318347986
Epoch:  7 Step:  100  | train loss: 1.4064  |test accuray:  0.6136396494392543
Epoch:  8 Step:  0  | train loss: 1.3919  |test accuray:  0.6094538494901999
Epoch:  8 Step:  10  | train loss: 1.3872  |test accuray:  0.609381561826606
Epoch:  8 Step:  20  | train loss: 1.3883  |test accuray:  0.6138324165421712
Epoch:  8 Step:  30  | train loss: 1.3887  |test accuray:  0.6159253165166985
Epoch:  8 Step:  40  | train loss: 1.3957  |test accuray:  0.6239699007937874
Epoch:  8 Step:  50  | train loss: 1.3901  |test accuray:  0.6276255912098201
Epoch:  8 Step:  60  | train loss: 1.3889  |test accuray:  0.6282383152155205
Epoch:  8 Step:  70  | train loss: 1.3930  |test accuray:  0.6300489490750621
Epoch:  8 Step:  80  | train loss: 1.3900  |test accuray:  0.6324619801312193
Epoch:  8 Step:  90  | train loss: 1.3946  |test accuray:  0.6369231616558694
Epoch:  8 Step:  100  | train loss: 1.3938  |test accuray:  0.6366718759681383
Epoch:  9 Step:  0  | train loss: 1.3803  |test accuray:  0.6363483026168134
Epoch:  9 Step:  10  | train loss: 1.3945  |test accuray:  0.6366822027772232
Epoch:  9 Step:  20  | train loss: 1.3952  |test accuray:  0.6397354959966404
Epoch:  9 Step:  30  | train loss: 1.3977  |test accuray:  0.6377630754614363
Epoch:  9 Step:  40  | train loss: 1.3881  |test accuray:  0.6351503927629721
Epoch:  9 Step:  50  | train loss: 1.3919  |test accuray:  0.6367648172499019
Epoch:  9 Step:  60  | train loss: 1.3891  |test accuray:  0.6392260400817883
Epoch:  9 Step:  70  | train loss: 1.3894  |test accuray:  0.6395220752755537
Epoch:  9 Step:  80  | train loss: 1.3839  |test accuray:  0.638417106703476
Epoch:  9 Step:  90  | train loss: 1.3916  |test accuray:  0.6389506585061926
Epoch:  9 Step:  100  | train loss: 1.3823  |test accuray:  0.6404962375992235
finish training in  384.456182718277
train loss: 1.3823  |test accuray:  0.6404962375992235
```


***Case 3***

```
Simple Encoding
Epoch = 10
BATCHSIZE = 2900
LR = 0.005
num_worker = 4
ANN(54, 100, 100, 7)
```

```
Epoch:  0 Step:  0  | train loss: 1.9440  |test accuray:  0.4866233399654396
Epoch:  0 Step:  10  | train loss: 1.5037  |test accuray:  0.4866233399654396
Epoch:  0 Step:  20  | train loss: 1.4946  |test accuray:  0.4712363944290307
Epoch:  0 Step:  30  | train loss: 1.4891  |test accuray:  0.3991449402077754
Epoch:  0 Step:  40  | train loss: 1.4638  |test accuray:  0.4384866405513139
Epoch:  0 Step:  50  | train loss: 1.4458  |test accuray:  0.500068845393899
Epoch:  0 Step:  60  | train loss: 1.4181  |test accuray:  0.5174316537352068
Epoch:  0 Step:  70  | train loss: 1.4143  |test accuray:  0.5250046470640882
Epoch:  0 Step:  80  | train loss: 1.4172  |test accuray:  0.5303814723275939
Epoch:  0 Step:  90  | train loss: 1.4178  |test accuray:  0.530054456706574
Epoch:  0 Step:  100  | train loss: 1.3951  |test accuray:  0.5297308833552491
Epoch:  1 Step:  0  | train loss: 1.4140  |test accuray:  0.5292799460252112
Epoch:  1 Step:  10  | train loss: 1.4000  |test accuray:  0.5310182922211589
Epoch:  1 Step:  20  | train loss: 1.4112  |test accuray:  0.5321852216477456
Epoch:  1 Step:  30  | train loss: 1.4075  |test accuray:  0.5458923395730209
Epoch:  1 Step:  40  | train loss: 1.4065  |test accuray:  0.5464603140726869
Epoch:  1 Step:  50  | train loss: 1.4127  |test accuray:  0.5455377857944415
Epoch:  1 Step:  60  | train loss: 1.4140  |test accuray:  0.5553138317280882
Epoch:  1 Step:  70  | train loss: 1.4128  |test accuray:  0.5705768555554791
Epoch:  1 Step:  80  | train loss: 1.3986  |test accuray:  0.5830034491542343
Epoch:  1 Step:  90  | train loss: 1.4084  |test accuray:  0.5708281412432101
Epoch:  1 Step:  100  | train loss: 1.4026  |test accuray:  0.5887348281963195
Epoch:  2 Step:  0  | train loss: 1.3958  |test accuray:  0.5782324633570392
Epoch:  2 Step:  10  | train loss: 1.4084  |test accuray:  0.5933405850481573
Epoch:  2 Step:  20  | train loss: 1.4079  |test accuray:  0.6136912834846785
Epoch:  2 Step:  30  | train loss: 1.4064  |test accuray:  0.6507473167507728
Epoch:  2 Step:  40  | train loss: 1.4078  |test accuray:  0.6522722422256338
Epoch:  2 Step:  50  | train loss: 1.4158  |test accuray:  0.6527438331738415
Epoch:  2 Step:  60  | train loss: 1.4102  |test accuray:  0.6391021183727703
Epoch:  2 Step:  70  | train loss: 1.4063  |test accuray:  0.6413912277199094
Epoch:  2 Step:  80  | train loss: 1.4064  |test accuray:  0.6306479040019828
Epoch:  2 Step:  90  | train loss: 1.3998  |test accuray:  0.6246514701933867
Epoch:  2 Step:  100  | train loss: 1.3943  |test accuray:  0.6101147652716296
Epoch:  3 Step:  0  | train loss: 1.4129  |test accuray:  0.6127859665549077
Epoch:  3 Step:  10  | train loss: 1.3992  |test accuray:  0.6074745444156059
Epoch:  3 Step:  20  | train loss: 1.4018  |test accuray:  0.6148857510688247
Epoch:  3 Step:  30  | train loss: 1.3995  |test accuray:  0.6080597302637467
Epoch:  3 Step:  40  | train loss: 1.4002  |test accuray:  0.6072714505036041
Epoch:  3 Step:  50  | train loss: 1.4012  |test accuray:  0.6050890515170082
Epoch:  3 Step:  60  | train loss: 1.3985  |test accuray:  0.6045795956021562
Epoch:  3 Step:  70  | train loss: 1.3998  |test accuray:  0.6032715331180767
Epoch:  3 Step:  80  | train loss: 1.3942  |test accuray:  0.6047585936262935
Epoch:  3 Step:  90  | train loss: 1.3987  |test accuray:  0.6026519245729864
Epoch:  3 Step:  100  | train loss: 1.4007  |test accuray:  0.6025693101003078
Epoch:  4 Step:  0  | train loss: 1.3963  |test accuray:  0.6020219892188113
Epoch:  4 Step:  10  | train loss: 1.3886  |test accuray:  0.6025452142124431
Epoch:  4 Step:  20  | train loss: 1.3899  |test accuray:  0.602552098751833
Epoch:  4 Step:  30  | train loss: 1.3934  |test accuray:  0.6027896153607842
Epoch:  4 Step:  40  | train loss: 1.3892  |test accuray:  0.6027070008881056
Epoch:  4 Step:  50  | train loss: 1.3990  |test accuray:  0.6026691359214612
Epoch:  4 Step:  60  | train loss: 1.3959  |test accuray:  0.6016054745857229
Epoch:  4 Step:  70  | train loss: 1.3907  |test accuray:  0.6023249089519666
Epoch:  4 Step:  80  | train loss: 1.3940  |test accuray:  0.6035469146936724
Epoch:  4 Step:  90  | train loss: 1.3979  |test accuray:  0.6029273061485821
Epoch:  4 Step:  100  | train loss: 1.3890  |test accuray:  0.602490137897324
Epoch:  5 Step:  0  | train loss: 1.3930  |test accuray:  0.6026002905275623
Epoch:  5 Step:  10  | train loss: 1.3909  |test accuray:  0.6053782021713837
Epoch:  5 Step:  20  | train loss: 1.3841  |test accuray:  0.6022216408611182
Epoch:  5 Step:  30  | train loss: 1.4043  |test accuray:  0.603443646602824
Epoch:  5 Step:  40  | train loss: 1.3958  |test accuray:  0.6063558067647484
Epoch:  5 Step:  50  | train loss: 1.3942  |test accuray:  0.6044522316234432
Epoch:  5 Step:  60  | train loss: 1.3885  |test accuray:  0.6036742786723854
Epoch:  5 Step:  70  | train loss: 1.3943  |test accuray:  0.604531403826427
Epoch:  5 Step:  80  | train loss: 1.3980  |test accuray:  0.6065520161373603
Epoch:  5 Step:  90  | train loss: 1.4032  |test accuray:  0.6071406442551961
Epoch:  5 Step:  100  | train loss: 1.3808  |test accuray:  0.6045864801415461
Epoch:  6 Step:  0  | train loss: 1.3969  |test accuray:  0.6033920125573998
Epoch:  6 Step:  10  | train loss: 1.3989  |test accuray:  0.6053162413168747
Epoch:  6 Step:  20  | train loss: 1.3979  |test accuray:  0.6057052177924036
Epoch:  6 Step:  30  | train loss: 1.3974  |test accuray:  0.6058876580862358
Epoch:  6 Step:  40  | train loss: 1.3878  |test accuray:  0.6063144995284091
Epoch:  6 Step:  50  | train loss: 1.3965  |test accuray:  0.6110235244710953
Epoch:  6 Step:  60  | train loss: 1.3908  |test accuray:  0.605743082759048
Epoch:  6 Step:  70  | train loss: 1.3917  |test accuray:  0.6118531114675773
Epoch:  6 Step:  80  | train loss: 1.3880  |test accuray:  0.6083179004908676
Epoch:  6 Step:  90  | train loss: 1.3983  |test accuray:  0.6097292310657956
Epoch:  6 Step:  100  | train loss: 1.3885  |test accuray:  0.604596806950631
Epoch:  7 Step:  0  | train loss: 1.3877  |test accuray:  0.6045589419839865
Epoch:  7 Step:  10  | train loss: 1.3931  |test accuray:  0.6088583368329742
Epoch:  7 Step:  20  | train loss: 1.3853  |test accuray:  0.6083936304241565
Epoch:  7 Step:  30  | train loss: 1.3902  |test accuray:  0.6079323662850337
Epoch:  7 Step:  40  | train loss: 1.3898  |test accuray:  0.6121353775825629
Epoch:  7 Step:  50  | train loss: 1.3899  |test accuray:  0.6072921041217737
Epoch:  7 Step:  60  | train loss: 1.3951  |test accuray:  0.6084418221998857
Epoch:  7 Step:  70  | train loss: 1.3834  |test accuray:  0.6093023896236223
Epoch:  7 Step:  80  | train loss: 1.3847  |test accuray:  0.6086518006512774
Epoch:  7 Step:  90  | train loss: 1.3920  |test accuray:  0.6071991628400102
Epoch:  7 Step:  100  | train loss: 1.3784  |test accuray:  0.6108169882893985
Epoch:  8 Step:  0  | train loss: 1.3833  |test accuray:  0.611295463776996
Epoch:  8 Step:  10  | train loss: 1.3929  |test accuray:  0.6102903210260717
Epoch:  8 Step:  20  | train loss: 1.3913  |test accuray:  0.6110476203589599
Epoch:  8 Step:  30  | train loss: 1.3900  |test accuray:  0.6122420879431062
Epoch:  8 Step:  40  | train loss: 1.3927  |test accuray:  0.6099151136293226
Epoch:  8 Step:  50  | train loss: 1.3885  |test accuray:  0.6119667063675105
Epoch:  8 Step:  60  | train loss: 1.3874  |test accuray:  0.6113505400921151
Epoch:  8 Step:  70  | train loss: 1.3867  |test accuray:  0.6132541152334203
Epoch:  8 Step:  80  | train loss: 1.3908  |test accuray:  0.6105657026016674
Epoch:  8 Step:  90  | train loss: 1.3939  |test accuray:  0.614290238411599
Epoch:  8 Step:  100  | train loss: 1.4060  |test accuray:  0.6094056577144706
Epoch:  9 Step:  0  | train loss: 1.3870  |test accuray:  0.6082249592091041
Epoch:  9 Step:  10  | train loss: 1.3859  |test accuray:  0.6109271409196367
Epoch:  9 Step:  20  | train loss: 1.3921  |test accuray:  0.6120011290644599
Epoch:  9 Step:  30  | train loss: 1.3974  |test accuray:  0.6095640021204382
Epoch:  9 Step:  40  | train loss: 1.3888  |test accuray:  0.6096328475143371
Epoch:  9 Step:  50  | train loss: 1.3902  |test accuray:  0.6160595650348013
Epoch:  9 Step:  60  | train loss: 1.3903  |test accuray:  0.6115398649253372
Epoch:  9 Step:  70  | train loss: 1.3917  |test accuray:  0.6110200822014004
Epoch:  9 Step:  80  | train loss: 1.3909  |test accuray:  0.6100734580352901
Epoch:  9 Step:  90  | train loss: 1.3883  |test accuray:  0.615694684447137
Epoch:  9 Step:  100  | train loss: 1.4060  |test accuray:  0.6140974713086821
finish training in  366.86503314971924
train loss: 1.4060  |test accuray:  0.6140974713086821
```

***case 4***

```
Simple Encoding
Epoch = 10
BATCHSIZE = 2900
LR = 0.0005
num_worker = 4
ANN(54, 100, 100, 7)
```

```
Epoch:  0 Step:  0  | train loss: 1.9505  |test accuray:  0.04289412266872285
Epoch:  0 Step:  10  | train loss: 1.9208  |test accuray:  0.45592517882591066
Epoch:  0 Step:  20  | train loss: 1.8798  |test accuray:  0.4879141911010444
Epoch:  0 Step:  30  | train loss: 1.8200  |test accuray:  0.4866233399654396
Epoch:  0 Step:  40  | train loss: 1.7305  |test accuray:  0.4866233399654396
Epoch:  0 Step:  50  | train loss: 1.6391  |test accuray:  0.4866233399654396
Epoch:  0 Step:  60  | train loss: 1.5562  |test accuray:  0.4866233399654396
Epoch:  0 Step:  70  | train loss: 1.5109  |test accuray:  0.4866233399654396
Epoch:  0 Step:  80  | train loss: 1.4997  |test accuray:  0.4866233399654396
Epoch:  0 Step:  90  | train loss: 1.4964  |test accuray:  0.4866233399654396
Epoch:  0 Step:  100  | train loss: 1.4976  |test accuray:  0.4866233399654396
Epoch:  1 Step:  0  | train loss: 1.4929  |test accuray:  0.4866233399654396
Epoch:  1 Step:  10  | train loss: 1.4875  |test accuray:  0.4866233399654396
Epoch:  1 Step:  20  | train loss: 1.4765  |test accuray:  0.4866233399654396
Epoch:  1 Step:  30  | train loss: 1.4705  |test accuray:  0.4866233399654396
Epoch:  1 Step:  40  | train loss: 1.4777  |test accuray:  0.4866233399654396
Epoch:  1 Step:  50  | train loss: 1.4711  |test accuray:  0.4875424259739902
Epoch:  1 Step:  60  | train loss: 1.4565  |test accuray:  0.5138723468706327
Epoch:  1 Step:  70  | train loss: 1.4607  |test accuray:  0.5181992798771798
Epoch:  1 Step:  80  | train loss: 1.4544  |test accuray:  0.5182474716529091
Epoch:  1 Step:  90  | train loss: 1.4377  |test accuray:  0.5186020254314885
Epoch:  1 Step:  100  | train loss: 1.4419  |test accuray:  0.5258617722181298
Epoch:  2 Step:  0  | train loss: 1.4373  |test accuray:  0.5266293983601027
Epoch:  2 Step:  10  | train loss: 1.4368  |test accuray:  0.5289150654375468
Epoch:  2 Step:  20  | train loss: 1.4291  |test accuray:  0.5293763295766697
Epoch:  2 Step:  30  | train loss: 1.4229  |test accuray:  0.5299856113126751
Epoch:  2 Step:  40  | train loss: 1.4232  |test accuray:  0.5299718422338954
Epoch:  2 Step:  50  | train loss: 1.4337  |test accuray:  0.5299718422338954
Epoch:  2 Step:  60  | train loss: 1.4280  |test accuray:  0.5301439557186426
Epoch:  2 Step:  70  | train loss: 1.4217  |test accuray:  0.5300441298974892
Epoch:  2 Step:  80  | train loss: 1.4221  |test accuray:  0.5300372453580993
Epoch:  2 Step:  90  | train loss: 1.4259  |test accuray:  0.5311318871210922
Epoch:  2 Step:  100  | train loss: 1.4373  |test accuray:  0.5310286190302438
Epoch:  3 Step:  0  | train loss: 1.4202  |test accuray:  0.5309150241303106
Epoch:  3 Step:  10  | train loss: 1.4238  |test accuray:  0.5302196856519314
Epoch:  3 Step:  20  | train loss: 1.4169  |test accuray:  0.5309391200181752
Epoch:  3 Step:  30  | train loss: 1.4266  |test accuray:  0.5303057423943051
Epoch:  3 Step:  40  | train loss: 1.4251  |test accuray:  0.5304262218336282
Epoch:  3 Step:  50  | train loss: 1.4223  |test accuray:  0.5301611670671174
Epoch:  3 Step:  60  | train loss: 1.4112  |test accuray:  0.5307911024212925
Epoch:  3 Step:  70  | train loss: 1.4149  |test accuray:  0.5311559830089568
Epoch:  3 Step:  80  | train loss: 1.4168  |test accuray:  0.5302334547307113
Epoch:  3 Step:  90  | train loss: 1.4131  |test accuray:  0.5306706229819694
Epoch:  3 Step:  100  | train loss: 1.4157  |test accuray:  0.530188705224677
Epoch:  4 Step:  0  | train loss: 1.4051  |test accuray:  0.5301646093368123
Epoch:  4 Step:  10  | train loss: 1.4182  |test accuray:  0.5303608187094242
Epoch:  4 Step:  20  | train loss: 1.4120  |test accuray:  0.5307945446909874
Epoch:  4 Step:  30  | train loss: 1.4088  |test accuray:  0.5311181180423123
Epoch:  4 Step:  40  | train loss: 1.4102  |test accuray:  0.5304881826881372
Epoch:  4 Step:  50  | train loss: 1.4069  |test accuray:  0.5309115818606156
Epoch:  4 Step:  60  | train loss: 1.4017  |test accuray:  0.5303263960124748
Epoch:  4 Step:  70  | train loss: 1.4150  |test accuray:  0.5305226053850867
Epoch:  4 Step:  80  | train loss: 1.4107  |test accuray:  0.5305467012729513
Epoch:  4 Step:  90  | train loss: 1.4079  |test accuray:  0.5303298382821697
Epoch:  4 Step:  100  | train loss: 1.4074  |test accuray:  0.5309735427151246
Epoch:  5 Step:  0  | train loss: 1.4062  |test accuray:  0.531011407681769
Epoch:  5 Step:  10  | train loss: 1.4084  |test accuray:  0.5309597736363448
Epoch:  5 Step:  20  | train loss: 1.4090  |test accuray:  0.5305157208456969
Epoch:  5 Step:  30  | train loss: 1.4104  |test accuray:  0.5304675290699675
Epoch:  5 Step:  40  | train loss: 1.4183  |test accuray:  0.5303642609791192
Epoch:  5 Step:  50  | train loss: 1.4132  |test accuray:  0.5305019517669171
Epoch:  5 Step:  60  | train loss: 1.4113  |test accuray:  0.5305570280820362
Epoch:  5 Step:  70  | train loss: 1.4068  |test accuray:  0.5308599478151914
Epoch:  5 Step:  80  | train loss: 1.3943  |test accuray:  0.5305673548911211
Epoch:  5 Step:  90  | train loss: 1.4115  |test accuray:  0.530694718869834
Epoch:  5 Step:  100  | train loss: 1.3879  |test accuray:  0.5308978127818358
Epoch:  6 Step:  0  | train loss: 1.4082  |test accuray:  0.5309425622878702
Epoch:  6 Step:  10  | train loss: 1.4137  |test accuray:  0.5310286190302438
Epoch:  6 Step:  20  | train loss: 1.3976  |test accuray:  0.5303229537427798
Epoch:  6 Step:  30  | train loss: 1.4061  |test accuray:  0.5313040006058395
Epoch:  6 Step:  40  | train loss: 1.4062  |test accuray:  0.5312145015937708
Epoch:  6 Step:  50  | train loss: 1.4054  |test accuray:  0.5310458303787186
Epoch:  6 Step:  60  | train loss: 1.4046  |test accuray:  0.5312489242907203
Epoch:  6 Step:  70  | train loss: 1.4067  |test accuray:  0.5305329321941715
Epoch:  6 Step:  80  | train loss: 1.4015  |test accuray:  0.5307325838364784
Epoch:  6 Step:  90  | train loss: 1.4063  |test accuray:  0.5303814723275939
Epoch:  6 Step:  100  | train loss: 1.4127  |test accuray:  0.5308943705121408
Epoch:  7 Step:  0  | train loss: 1.4123  |test accuray:  0.530890928242446
Epoch:  7 Step:  10  | train loss: 1.4103  |test accuray:  0.5311181180423123
Epoch:  7 Step:  20  | train loss: 1.4135  |test accuray:  0.5309941963332944
Epoch:  7 Step:  30  | train loss: 1.4034  |test accuray:  0.530625873475935
Epoch:  7 Step:  40  | train loss: 1.4104  |test accuray:  0.5304090104851535
Epoch:  7 Step:  50  | train loss: 1.4125  |test accuray:  0.531334981033094
Epoch:  7 Step:  60  | train loss: 1.4096  |test accuray:  0.5307394683758683
Epoch:  7 Step:  70  | train loss: 1.4111  |test accuray:  0.5310286190302438
Epoch:  7 Step:  80  | train loss: 1.4157  |test accuray:  0.5314210377754677
Epoch:  7 Step:  90  | train loss: 1.4063  |test accuray:  0.5310320612999387
Epoch:  7 Step:  100  | train loss: 1.4074  |test accuray:  0.5314795563602818
Epoch:  8 Step:  0  | train loss: 1.4086  |test accuray:  0.5314244800451626
Epoch:  8 Step:  10  | train loss: 1.4061  |test accuray:  0.53094944682726
Epoch:  8 Step:  20  | train loss: 1.4076  |test accuray:  0.5312385974816355
Epoch:  8 Step:  30  | train loss: 1.4091  |test accuray:  0.5314348068542474
Epoch:  8 Step:  40  | train loss: 1.4119  |test accuray:  0.5313177696846193
Epoch:  8 Step:  50  | train loss: 1.4081  |test accuray:  0.5311938479756012
Epoch:  8 Step:  60  | train loss: 1.4091  |test accuray:  0.5316069203389947
Epoch:  8 Step:  70  | train loss: 1.4107  |test accuray:  0.5315759399117402
Epoch:  8 Step:  80  | train loss: 1.4072  |test accuray:  0.5315484017541806
Epoch:  8 Step:  90  | train loss: 1.4068  |test accuray:  0.5314485759330272
Epoch:  8 Step:  100  | train loss: 1.3820  |test accuray:  0.530825525118242
Epoch:  9 Step:  0  | train loss: 1.4112  |test accuray:  0.530887485972751
Epoch:  9 Step:  10  | train loss: 1.4083  |test accuray:  0.5316860925419785
Epoch:  9 Step:  20  | train loss: 1.3965  |test accuray:  0.5317377265874027
Epoch:  9 Step:  30  | train loss: 1.4045  |test accuray:  0.531717072969233
Epoch:  9 Step:  40  | train loss: 1.3995  |test accuray:  0.5318306678691662
Epoch:  9 Step:  50  | train loss: 1.4119  |test accuray:  0.531971800926659
Epoch:  9 Step:  60  | train loss: 1.4061  |test accuray:  0.5318891864539803
Epoch:  9 Step:  70  | train loss: 1.3979  |test accuray:  0.5320888380962872
Epoch:  9 Step:  80  | train loss: 1.4044  |test accuray:  0.5320750690175073
Epoch:  9 Step:  90  | train loss: 1.3975  |test accuray:  0.5322437402325597
Epoch:  9 Step:  100  | train loss: 1.3799  |test accuray:  0.5327153311807673
finish training in  368.58473014831543
train loss: 1.3799  |test accuray:  0.5327153311807673
```
