# 1. Preprocessing Data

+ Simple encoding: just normalise all features to [0,1]
+ Complex encoding:
	- normalise all continuous to [0,1]. *equalise or categories in the feature*
	- for circular value, use sin and cos
	- for indexed value, since it is truncted at 255, using two variables, saturate and value
	- for 40 bits soil type, using 3-d embedding encoding
	- for 4 bits area, use one hot
	- for output, unchange. minus 1 for each category index later in the pytorch code. *equilateral encoding in the futrue*

+ training/tesint set
	- take 50% as training set, 25% as testing set, 25% as validation set.
	- the pattern distribution should be the same between training set and testing set. however, here we don't care about it. just randomly seperate whole dataset into training and testing.
	- *we should add noise to avoid overfitting, neglect this step*


# 2. Hyper Parameter

```
Simple Encoding/Complex Encoding
Epoch = 10
BATCHSIZE = 100
LR = 0.001
num_worker = 4
ANN(54, 100, 100, 7)
```

1. use CPU, rather than GPU
2. LR = 0.01
2. BATCHSIZE = 290
2. EPOCH = 20
2. nhidden fix 100

## Improve GPU utilization

+ actually, GPU 36s for one epoch, CPU 6s for one epoch
+ increase batch size: BATCHSIZE = 100 and BATCHSIZE = 2900, both 25% utilization.
+ increase number of worker in dataloader: num\_worker = 4, 16. both 25% utilization.
+ more big neuron network: 

## Learning rate

+ LR. ~~~from README\_old.md~~~ EPOCH 10, BS 290, nhidden 100
	- accuarcy of 0.05 is 70%, loss 1.35, < 1 epoch converge
	- **accuarcy of 0.01 is 72%**, loss 1.37, < 1 epoch converge
	- accuarcy of 0.005 is 69%, loss 1.36, < 1 epoch converge
	- accuarcy of 0.001 is 65%, loss 1.38, < 1 epoch converge
	- accuarcy of 0.0005 is 65%, loss 1.37, < 1 epoch converge
	
## BATCHSIZE

+ BATCHSIZE. EPOCH 10, LR 0.01, nhidden 100
	- **accuarcy of 290 is 73%**, loss 1.37 , accuarcy increase extremely slow, converge at 8 epoch
	- accuarcy of 29 is 69%, loss 1.38, flunctuate greatly. mini batch size is too small
	- accuarcy of 2900 is 67%, loss 1.37, accuarcy has two stages, converge after 5 epoches.
	- accuarcy of 29000 is 63%, loss 1.41,  accuarcy has two stages, converge after 3 epoches.
	- accuarcy of 29000 is 69%, loss 1.37,  accuarcy has converge after 50 epoches. (EPOCH 100)

## EPOCH

+ EPOCH. BATCHSIZE = 290, LR 0.01 nhidden 100
	- EPOCH 10 , accuarcy 72%, loss function 1.39, still flunctuates.  71 seconds ![epoch10](epoch10.png)
	- EPOCH 100, accuarcy 74.9%, loss function 1.37. EPOCH = 20 is enough. 724 seconds. ![epoch100](epoch100.png).
	- EPOCH 20 , accuarcy 72.9%, loss function 1.34,  150 seconds ![epoch20](epoch20.png)

## nhidden

we fix nhidden = 100, since our subject is pruning.
we will not fine tuning nhidden.

# 3. Simple encoding and Complex encoding, which is better

complex encoding test 70.6%,  valid 63.6%, but only 20 input
simple encoding test 71.9%, valid 62.8%, have 54 input!



# 4. Pruning

+ remove constant neurons
+ remove complementary neuron pairs
+ remove one neuron from similiarty pair

+ slow for large mount of hidden neurons. lots to improve

