## Task Decomposition

1. pick up a paper P1 **Network Reduction Techniques**
	- [x] read the paper
	- make the outline of pytorch
	- [x] make the outline of my report structure 
2. select a dataset
	- [x] [covertype data set](https://archive.ics.uci.edu/ml/datasets/Covertype)
3. preprocess the dataset (one .py)
	- [x] about input/output encoding scheme
	- [x] a python module to preprocess dataset
	- [x] save the preprocessed dataset into hard disk
4. testing module (one .py)
	- which measure(s) to evaluate (use the measure in P2)
5. vanilla training (one .py)
	- network architecture
	- training
	- show its performace using testing module
6. improved training (one .py)
	- implement the method in P1
	- show its performace using testing module
7. find a research paper P2 using my dataset and compare results

8. my own improvement

9. report composing
	- modify the margins in LaTeX and Springer citation style

## Report Structure

- introduction
- method/design
	+ vanilla
	+ improved from Tom's method
		- distinctiveness angles calculation, and how to prune
		- single constant neuron
	+ my own improvement
		- angle thredshold effect
		- single constant neuron thredshold effect.
		- group of three ~~~or more~~~ together have no effect
		- two ~~~or more~~~ with a constant effect
- implementation
	+ prune: 
		1. remove single constant neuron
		2. remove complementary
		3. remove similar
- result and experiment
	- vanilla training: (fig) loss-iteration diagram (show convergence)
	- output in pattern space (table), and angles of pair
	- *which evaluation, from P2*, accuracy
	- compare the result of vanilla to Tom's method
	- result of my own improvement
	- compare my own improvement with Tom's method
	- compare P2's result
- further discussion
	- group of three ~~~or more~~~ together have no effect
	- two ~~~or more~~~ with a constant effect
	
- conclusion



